package main

import (
	"encoding/json"
	"fmt"
)

type User struct {
	Email    string
	Password string `json:"-"`
}

type AuthedUser struct {
	Email    string
	Password string `json:"password"`
}

type UserResp struct {
	UserDetails User `json:"user"`
}

type AuthedUserResp struct {
	UserDetails AuthedUser `json:"user"`
}

func main() {
	fmt.Println("hello.")

	u := User{
		"wg@wadegasior.com",
		"mypassword!",
	}

	resp := UserResp{
		UserDetails: u,
	}

	respJSON, _ := json.Marshal(resp)
	fmt.Println(string(respJSON))

	authedResp := AuthedUserResp{
		UserDetails: AuthedUser(u),
	}

	respJSON, _ = json.Marshal(authedResp)
	fmt.Println(string(respJSON))

}
